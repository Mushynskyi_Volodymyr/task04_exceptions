package com.epam.model;

import java.io.IOException;

public class BusinessLogic implements Model{
    private Car Porsche_911;
    private Car Ford_Mustang;
    private Car Lamborghini_Huracan;
    private Car Bugatti_Veyron;
    private Car MacLaren_650S;
    private int exception = 1;

    public BusinessLogic() throws IOException {
        try {
            Porsche_911= new Car("Porsche", "911", 2018, "Yellow", 200000, "AA 5834 RT");
            System.out.println("New auto added");
            MacLaren_650S = new Car("MacLaren", "650S", 2019, "Black", 210000, "RR 1547 PK");
            System.out.println("New auto added");
            Lamborghini_Huracan = new Car("Lamborghini", "Huracan", 2019, "Green", 250000, "RE 9453 YO");
            System.out.println("New auto added");
            Bugatti_Veyron = new Car("Bugatti", "Veyron", 2017, "Brown", 150000, "VE 8457 NE");
            System.out.println("New auto added");
            if (exception == 1) {
                throw new IOException("There was an exception");
            }
            Ford_Mustang = new Car("Ford", "Mustang", 2018, "Blue", 125000, "FT 5497 TY");
            System.out.println("New auto added");
        } catch (IOException e) {
            System.out.println("Test run");
        }
    }
}

