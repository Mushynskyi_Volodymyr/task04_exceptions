package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.Control;

import java.io.IOException;

public class Myview {
    private Controller controller;

    public Myview() throws IOException {
        controller = new Control();
        controller.start();
    }
}

